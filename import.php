<?php
/**
 * Produces a HTML document to add CSV files to the database.
 *
 * @package    ProjectToKML
 * @subpackage SurveyingKML
 * @license    http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author     Robert Flood <robert.d.flood@gmail.com>
 */

namespace ProjectToKML;
include_once 'department.php';
include_once 'project.php';

if (isset($_FILES['data'])) {
    $errors = array();
    $file_name = $_FILES['data']['name'];
    $file_size = $_FILES['data']['size'];
    $file_tmp = $_FILES['data']['tmp_name'];
    $file_type = $_FILES['data']['type'];
    $file_ext = "csv";

    $expensions = array("txt", "csv");

    if (in_array($file_ext, $expensions) === false) {
        $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
    }

    if ($file_size > 2097152) {
        $errors[] = 'File size must be less than 2MB';
    }

    if (empty($errors) == true) {
        move_uploaded_file($file_tmp, "data/full_data.csv");
        // echo "Success";
    } else {
        print_r($errors);
    }
}
?>
<html>
<head>
    <title>WCG Survey KML Generator</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="default.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="index.php">Surveying KML Generator</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Add Single Projects</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="import.php">CSV Import <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://unionsquare.wolterconsulting.com.au">UnionSquare</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="margin-top: 25px">
    <div class="card">
        <div class="card-header">
            <strong>Load a CSV (From UnionSquare)</strong>
        </div>
        <div class="card-body">
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <input type="file" name="data"/>
                    <input class="btn btn-success" type="submit"/>
                    <?php
                    if (isset($_FILES['data'])) {

                        echo "<div class=\"card\" style='margin-top: 25px'>
        <div class=\"card-header\">
            File Details
        </div>
        <div class=\"card-body\">";
                        echo "    <ul>";
                        echo "        <li>Sent file: $file_name </li>";
                        echo "        <li>File size: $file_size </li>";
                        echo "        <li>File type: $file_type </li>";
                        echo "    </ul></div>";
                    }
                    ?>
                </div>
        </div>
    </form>
    </div>
</div>


<div class="container" style="margin-top: 25px">
    <div class="card">
        <div class="card-header">
            <strong>Details</strong>
        </div>
        <div class="card-body">

            <?php
            try {
                if (isset($_FILES['data'])) {
                    //echo " adding jobs ... ";
                    $jobs = new department();
                    $jobs->csvFileImport('data/full_data.csv');
                    $jobs->fileExport();

                    echo '<a class="btn btn-warning" href="data/surveyjobs.kml">KML File Download</a></div>';

                } else if ((isset($_FILES['data'])) && (isset($_POST['dload']))) {
                    //echo " exporting jobs ...";
                    $jobs = new department();
                    $jobs->generatejobsKML();
                } else {
                    echo "<div class=\"alert alert-warning\" role=\"alert\">Load a CSV file with order - \"CODE, DESCRIPTION, ADDRESS, MANAGER, US ENTITY CODE\" </div>";
                }
            } catch (\Exception $addException) {
                echo "<div class=\"alert alert-danger\" role=\"alert\">Could not load CSV. </div>";
            }
            ?>
        </div>
    </div>
</div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<footer class="footer">
    <div class="container">
        <span class="text-muted">Wolter Consulting Group</span>
    </div>
</footer>
</body>
</html>