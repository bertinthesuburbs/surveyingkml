<?php
/**
 * This file the class specification for a department of projects, currently for surveying projects.
 *
 * @package    ProjectToKML
 * @subpackage SurveyingKML
 * @license    http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author     Robert Flood <robert.d.flood@gmail.com>
 */

// TODO: Implement checking for doubles in database
// TODO: Reduce variable storing

namespace ProjectToKML;
include_once 'processes.php';
use Exception;

/**
 * Defines a "department" which can contain multiple projects from the same discipline.
 */
class department
{
    private $departmentProjects;                            //The array of historical and current projects.

    /**
     * Reads all historical projects currently in the database and starts a new array.
     * department constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->departmentProjects = array();
        try {
            $this->mainFileImport();
        } catch (\Exception $importException) {
            echo 'Caught exception: ' . $importException->getMessage(), "\n";
            throw new Exception($importException->getMessage());
        }
    }

    /**
     * Add a new project to the department list manually, catch exceptions and do not add the affecting project to
     * the department.
     * @param $projectCode Project code recorded in UnionSquare or TimeBill.
     * @param $projectName Project name, usually the address of the project.
     * @param $projectAddress Project address of the project, unformatted.
     * @param $projectManager Project manager of the project
     * @param $projectEntityCode Project UnionSquare entity code for direct linking to project.
     */
    public function addNewProject($projectCode, $projectName, $projectAddress, $projectManager, $projectEntityCode)
    {
        // Try to add a new project to the department, throw a new exception if it cannot add a new project.
        try {
            $tempjob = new project($projectCode, $projectName, $projectAddress, null, null, $projectManager, $projectEntityCode);

            if ($this->compareProjects($tempjob)) {
                $this->departmentProjects[] = $tempjob;
            } else {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> ' . $projectCode . ' - ' . $projectAddress . ':  <em>Project Code already exists.</em><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }

        } catch (\Exception $newJobException) {
            // Catch projects that cannot be added, display a message to the console.
            // TODO: Add html formatted console exception (via function) to this part.
            //echo 'Caught exception: ' . $newJobException->getMessage(), "\n";
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> ' . $projectCode . ' - ' . $projectAddress . ':  <em>' . $newJobException->getMessage() .'</em><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            //throw new Exception('Could not add the project, check the details of the project.');
        }
    }

    /**
     * Imports a csv formatted with the following information; Project Code, Project Address,
     * Project Description, Latitude, Longitude and Project Manager.
     *
     * Add the projects as objects for kml generation.
     *
     * @param $filename
     */
    public function mainFileImport() {
        $projects = array_map('str_getcsv', file('data/joblist.csv'));

        foreach($projects as $project) {
            if(isset($project[1])) {
                try {
                    $this->addHistoricalProject($project[0], $project[1], $project[2], $project[3], $project[4], $project[5], $project[6]);
                } catch (\Exception $csvLineException) {
                    //echo 'Could not add ' . $project[0] . ', not a valid project.';
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert"><strong>Error!</strong> Could not add ' . $project[0] . ', not a valid project.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

                }
            }
        }
    }

    /**
     * Add a previously added project from a file, must include all correct details. Throws an exception if the details
     * are not legitimate.
     * @param $projectCode Project code recorded in UnionSquare or TimeBill.
     * @param $projectName Project name, usually the address of the project.
     * @param $projectAddress Project address of the project, formatted by Google API.
     * @param $projectLatitude Project latitude discovered by Google API
     * @param $projectLongitude Project longitude discovered by Google API
     * @param $projectManager Project manager of the project
     * @param $projectEntityCode Project UnionSquare entity code for direct linking to project.
     * @throws Exception if the historical project does not have legitimate location data or is a duplicate.
     */
    public function addHistoricalProject($projectCode, $projectName, $projectAddress, $projectLatitude, $projectLongitude, $projectManager, $projectEntityCode)
    {
        // Try to add an existing project to the department, throw a new exception if it cannot add a new project.
        try {
            $tempjob = new project($projectCode, $projectName, $projectAddress, $projectLatitude, $projectLongitude, $projectManager, $projectEntityCode);

            if ($this->compareProjects($tempjob)) {
                $this->departmentProjects[] = $tempjob;
            }

            //TODO: Add address checks so that jobs at the same location share one KML placholder.
        } catch (\Exception $newJobException) {
            // Catch projects that cannot be added, display a message to the console.
            // TODO: Add html formatted console exception (via function) to this part.
            echo 'Caught exception: ' . $newJobException->getMessage(), "\n";
            throw new Exception('Could not add historical project ('.$projectCode.'), check the details of the project.');
        }
    }

    /**
     * Compares newly instated project with all projects currently added and returns true if a unique
     * @param project $newproject
     * @return bool
     */
    public function compareProjects(project $newproject) {
        $uniquejob = true;
        foreach ($this->departmentProjects as $project) {
            if ($newproject->isDuplicateJob($project)) {
                $uniquejob = false;
            }
        }
        return $uniquejob;
    }

    /**
     * Returns the array of all department jobs added to the environment.
     * @return array
     */
    public function getDepartmentProjects()
    {
        return $this->departmentProjects;
    }

    /**
     * Returns the number of projects currently in the department.
     * @return int
     */
    public function getNumberProjects() {
        return sizeof($this->departmentProjects);
    }

    /**
     * Imports a csv formatted with the following information; Project Code, Project Address,
     * Project Description, Project Manager.
     *
     * Add the projects as objects for kml generation.
     *
     * @param $filename
     */
    public function csvFileImport($filename) {
        if($this->getNumberProjects() == 0) {
            $this->mainFileImport();
        }
        $projects = array_map('str_getcsv', file($filename));

        foreach ($projects as $project) {
            if ($project != null) {
                $this->addNewProject($project[0], $project[1], $project[2], $project[3], $project[4]);
                echo "\n";
            }
        }
    }

    /**
     * Exports the current state as a csv for future generation of KML.
     * @return null|string
     */
    public function fileExport()
    {
        if ($this->getNumberProjects() == 0) {
            return null;
        } else {
            $jobexport = array();
            foreach ($this->departmentProjects as $job) {
                array_push($jobexport, $job->getCSVFormat());
            }
            $file = fopen('data/joblist.csv', 'w');
            fputcsv($file, array());
            foreach ($jobexport as $job) {
                fputcsv($file, $job);
            }
            fclose($file);
            $this->generatejobsKML();
            return;
        }
    }

    /**
     * Generates a kml file that can be imported to Google Earth / Queensland Globe
     */
    public function generatejobsKML() {
        $kmlprojects = array();
        foreach ($this->departmentProjects as $projects) {
                $kmlprojects[] = $projects;
        }
        generateKML($this->getDepartmentProjects());
    }
}