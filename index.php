<?php
/**
 * Produces a HTML document to add CSV files to the database.
 *
 * @package    ProjectToKML
 * @subpackage SurveyingKML
 * @license    http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author     Robert Flood <robert.d.flood@gmail.com>
 */

namespace ProjectToKML;
include_once 'department.php';
include_once 'project.php';
?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="default.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#">Surveying KML Generator</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Add Single Projects</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="import.php">CSV Import <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://unionsquare.wolterconsulting.com.au">UnionSquare</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container" style="margin-top: 25px">
    <div class="card">
        <div class="card-header">
            Add the following project to the KML
        </div>
        <div class="card-body">
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="code" class="col-4 col-form-label">Project Code:</label>
                    <div class="col-8">
                        <input id="code" name="code" placeholder="Enter project code..." required="required" class="form-control here" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="code" class="col-4 col-form-label">Project Name:</label>
                    <div class="col-8">
                        <input id="name" name="name" placeholder="Enter project name..." required="required" class="form-control here" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-4 col-form-label">Project Address:</label>
                    <div class="col-8">
                        <input id="address" name="address" placeholder="Add project address..." required="required" class="form-control here" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="manager" class="col-4 col-form-label">Project Manager:</label>
                    <div class="col-8">
                        <input id="manager" name="manager" placeholder="Add project manager..." required="required" class="form-control here" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="uscode" class="col-4 col-form-label">UnionSquare Entity Code:</label>
                    <div class="col-8">
                        <input id="uscode" name="uscode" placeholder="Add UnionSquare entity code..." required="required" class="form-control here" type="text">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="offset-4 col-8">
                        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 25px">
    <div class="card">
        <div class="card-header">
            Details
        </div>
        <div class="card-body">
            <?php
            if(isset($_POST['code'])) {

                $address = $_POST['address'];
                $code = $_POST['code'];
                $name = $_POST['name'];
                $manager = $_POST['manager'];
                $uscode = $_POST['uscode'];

                try {
                    $alljobs = new department();

                    $alljobs->addNewProject($code, $name, $address, $manager, $uscode);
                    $alljobs->fileExport();
                } catch (\Exception $newProjectException) {
                    echo '<div class=\"alert alert-danger\" role=\"alert\">Could add location. <em>'. $newProjectException->getMessage() .'</em></div>';
                }

            }

            echo '<a class="btn btn-warning" href="data/surveyjobs.kml">KML File Download</a></div>';
            ?>
        </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<footer class="footer">
    <div class="container">
        <span class="text-muted">Wolter Consulting Group</span>
    </div>
</footer>
</body>
</html>