<?php
/**
 * This file the class specification for a project, currently for surveying projects.
 *
 * @package    ProjectToKML
 * @subpackage SurveyingKML
 * @license    http://opensource.org/licenses/gpl-license.php  GNU Public License
 * @author     Robert Flood <robert.d.flood@gmail.com>
 */

// TODO: Optimise
// TODO: Check the address to make sure it contains a number, street, suburb, state and postcode.

namespace ProjectToKML;
use Exception;

/**
 * Defines a "project" that includes a project code, name, address, latitude, longitude, manager and entity code.
 */
class project
{
    private $projectCode;                               //The project code recorded in UnionSquare or TimeBill.
    private $projectName;                               //The project name, usually the address of the project.
    private $projectAddress;                            //The address of the project, formatted by google.
    private $projectLatitude;                           //The latitude of the project, between -12 and -44 degrees.
    private $projectLongitude;                          //The longitude of of the project, between 113 and 154 degrees.
    private $projectManager;                            //The project manager of the project
    private $projectEntityCode;

    //The UnionSquare entity code for direct linking to project.

    /**
     * Constructor function for projectJob, creates in instance of projectJob. Throws exception when location does not
     * reside within Australia, or Location cannot be found by Google API.
     *
     * To be used for new projects that do not have a geolocation provided.
     *
     * @param $projectCode Project code recorded in UnionSquare or TimeBill.
     * @param $projectName Project name, usually the address of the project.
     * @param $projectAddress Project address of the project, unformatted.
     * @param $projectManager Project manager of the project
     * @param $projectEntityCode Project UnionSquare entity code for direct linking to project.
     * @throws Exception if the project could not be added.
     */
    public function __construct1($projectCode, $projectName, $projectAddress, $projectManager, $projectEntityCode)
    {
        // Add the project details to the project, throw an exception if the location cannot be verified.
        try {
            $this->getGoogleGeolocation($projectAddress, $projectCode, $projectName, $projectManager);
            $this->checkAustralianLocation();
            $this->projectCode = $this->makeHTMLFriendly($projectCode);
            $this->projectName = $projectName;
            $this->projectAddress = $projectAddress;
            $this->projectManager = $projectManager;
            $this->projectEntityCode = $projectEntityCode;
        } catch (\Exception $geolocationException) {
            // Print the exception message to the console and throw a new exception.
            // TODO: Add html formatted console exception (via function) to this part.
            //echo 'Caught exception: ' . $geolocationException->getMessage(), "\n";
            throw new Exception($geolocationException->getMessage());
        }
    }

    /**
     * Constructor function for projectJob, creates in instance of projectJob. Throws exception when location does not
     * reside within Australia, or Location cannot be found by Google API.
     *
     * To be used for existing projects that already have a geolocation provided.
     *
     * @param $projectCode Project code recorded in UnionSquare or TimeBill.
     * @param $projectName Project name, usually the address of the project.
     * @param $projectAddress Project address of the project, formatted by Google API.
     * @param $projectLatitude Project latitude discovered by Google API (null id not available)
     * @param $projectLongitude Project longitude discovered by Google API (null id not available)
     * @param $projectManager Project manager of the project
     * @param $projectEntityCode Project UnionSquare entity code for direct linking to project.
     * @throws Exception is the job does not have a legitimate geolocation.
     */
    public function __construct($projectCode, $projectName, $projectAddress, $projectLatitude, $projectLongitude, $projectManager, $projectEntityCode)
    {
        try {
            if ($projectLatitude == null || $projectLongitude == null) {
                $this->getGoogleGeolocation($projectAddress, $projectCode, $projectName, $projectManager);
            } else {
                $this->projectLatitude = $projectLatitude;
                $this->projectLongitude = $projectLongitude;
                $this->checkAustralianLocation();
            }
            $this->projectCode = $this->makeHTMLFriendly($projectCode);
            $this->projectName = $projectName;
            $this->projectAddress = $projectAddress;
            $this->projectManager = $projectManager;
            $this->projectEntityCode = $projectEntityCode;

        } catch (\Exception $geolocationException) {
            //echo 'Caught exception: ' . $geolocationException->getMessage(), "\n";
            throw new Exception($geolocationException->getMessage());
        }
    }

    /**
     * Connects to GoogleAPI to search for geolocation from address. Project must have stored address in the class
     * before summoning this function.
     * @param $address
     * @param $code
     * @param $name
     * @param $manager
     * @return bool
     * @throws Exception
     */
    private function getGoogleGeolocation($address, $code, $name, $manager)
    {

        $address = urlencode($address);
        $apikey = "AIzaSyC_0VuHVcY88T5hieXZaanm7Xn0j4lWByo";
//        $apikey = "AIzaSyA74mgCW-v42Brruame2lKTPk9Ky7k9GxY";
        $url = "https://maps.google.com/maps/api/geocode/json?address={$address}&key=$apikey";
        $resp_json = file_get_contents($url);
        $resp = json_decode($resp_json, true);

        // Check the response from google.
        if ($resp['status'] == 'OK') {
            // Response from google was a success store the latitude, longitude and formatted address.
            $lati = $resp['results'][0]['geometry']['location']['lat'];
            $longi = $resp['results'][0]['geometry']['location']['lng'];
            $formatted_address = $resp['results'][0]['formatted_address'];

            // Check that all details have been recovered.
            if ($lati && $longi && $formatted_address && $this->checkNewAustralianLocation($lati, $longi)) {
                // Store the latitude, longitude and formatted address of the project.
                $this->setProjectLatitude($lati);
                $this->setProjectLongitude($longi);
                $this->setProjectAddress($formatted_address);
                return true;
            } else {
                // Do not store any of the details of the project and return false.
                // TODO: Add html formatted console exception (via function) to this part.
                $this->logErronousProject("$code", "$name", "$manager", "LOCATIONNOTFOUND");
                throw new Exception('Cannot find a valid geolocation for this address ('.$code.').');
            }
        } else {
            // Do not store anything for the project and return false.
            // TODO: Add html formatted console exception (via function) to this part.
            $this->logErronousProject("$code", "$name", "$manager", "UNKNOWNLOCATION");
            throw new Exception('Cannot connect to the Google geolocation API.');
        }
    }

    /**
     * Check that the location of the project is within Australian borders.
     * @throws Exception
     */
    private function checkAustralianLocation()
    {
        if ($this->projectLongitude > 154 || $this->projectLongitude < 113) {
            $this->logErronousProject($this->projectCode, $this->projectName, $this->projectManager, "NOTAUSLONG");
            throw new Exception('Longitude of location ('.$this->projectCode.') is not in Australia.');
        } else if ($this->projectLatitude > -12 || $this->projectLatitude < -44) {
            $this->logErronousProject($this->projectCode, $this->projectName, $this->projectManager, "NOTAUSLAT");
            throw new Exception('Latitude of location ('.$this->projectCode.') is not in Australia.');
        }
    }

    /**
     * Check that the location of latitude and longitude is within Australian borders
     * @param $latitude
     * @param $longitude
     * @return bool
     */
    private function checkNewAustralianLocation($latitude, $longitude)
    {
        if ($longitude > 154 || $longitude < 113) {
            return false;
        } else if ($latitude > -12 || $latitude < -44) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Removes formatting from project codes as to not cause invalid symbols in KML.
     * @param $parseString
     * @return mixed
     */
    private function makeHTMLFriendly($parseString) {
        $newString = str_replace('&', '&amp;', $parseString);
        $newString = str_replace('  ', ' ', $newString);

        return $newString;
    }

    /**
     * Checks to see if one job is identical to another.
     * @param project $proposedJob
     * @return bool
     */
    public function isDuplicateJob(project $proposedJob)
    {
        if ($this->getProjectCode() == $proposedJob->getProjectCode()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks to see if one job is identical to another. Returns false if there is a match.
     * @param $proposedCode Project code of the new project to be tested.
     * @return bool True if the proposed job is unique.
     */
    public function checkDuplicateCode($proposedCode)
    {
        if ($this->projectCode == $proposedCode) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @return mixed
     */
    public function getProjectCode()
    {
        return $this->projectCode;
    }

    /**
     * @return mixed
     */
    public function getProjectAddress()
    {
        return $this->projectAddress;
    }

    /**
     * @return mixed
     */
    public function getProjectLatitude()
    {
        return $this->projectLatitude;
    }

    /**
     * @return project
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @return project
     */
    public function getProjectManager()
    {
        return $this->projectManager;
    }

    /**
     * @return project
     */
    public function getProjectEntityCode()
    {
        return $this->projectEntityCode;
    }

    /**
     * @return mixed
     */
    public function getProjectLongitude()
    {
        return $this->projectLongitude;
    }

    /**
     * Returns an array of the job for CSV output.
     * @return array
     */
    public function getCSVFormat()
    {
        return [$this->projectCode, $this->projectAddress, $this->projectAddress, $this->projectLatitude,
            $this->projectLongitude, $this->projectManager, $this->projectEntityCode];
    }

    /**
     * @param mixed $projectAddress
     */
    public function setProjectAddress($projectAddress)
    {
        $this->projectAddress = $projectAddress;
    }

    /**
     * @param mixed $projectLatitude
     */
    public function setProjectLatitude($projectLatitude)
    {
        $this->projectLatitude = $projectLatitude;
    }

    /**
     * @param mixed $projectLongitude
     */
    public function setProjectLongitude($projectLongitude)
    {
        $this->projectLongitude = $projectLongitude;
    }

    /**
     * Logs an error to logs/notadded.txt appended with project code, name, manager and the error message.
     * @param $code - Project code of the erroneous entry.
     * @param $name - Project name as is in UnionSquare
     * @param $manager - Project manager as in UnionSquare
     * @param $message - Error message to record in
     */
    private function logErronousProject($code, $name, $manager, $message)
    {
        $logfile = fopen("logs/notadded.txt", "a+");
        $line = '"' . $code . '", "' . $name . '", "' . $manager . '", ' . $message . '"';
        //echo $line;
        fputs($logfile, "\n" . $line);
        fclose($logfile);
    }
}


