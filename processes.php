<?php
/**
 * Created by PhpStorm.
 * User: rflood
 * Date: 7/02/2018
 * Time: 3:31 PM
 */
namespace ProjectToKML;
include_once 'project.php';

function generateKML($jobarray) {
    // Creates an array of strings to hold the lines of the KML file.
    $kml = array('<?xml version="1.0" encoding="UTF-8"?>');
    $kml[] = '<kml xmlns="http://earth.google.com/kml/2.2">';
    $kml[] = ' <Document>';

    //echo "<br><br>OUTPUT KML<br><br>";
    //print_r($jobarray);

    $alljobs = array();

// Iterates through the rows, printing a node for each row.
    foreach ($jobarray as $singlejob) {
        $code = $singlejob->getProjectCode();
        $title = $singlejob->getProjectName();
        $address = $singlejob->getProjectAddress();
        $lat = $singlejob->getProjectLatitude();
        $long = $singlejob->getProjectLongitude();
        $owner = $singlejob->getProjectManager();
        $codeUS = $singlejob->getProjectEntityCode();
        $URLus = '"https://unionsquare.wolterconsulting.com.au/entity/entity.asp?ec=3&code=' . $codeUS . '"';

        $kml[] = ' <Placemark id="placemark-' . $code . '">';
        $kml[] = '    <name>' . htmlentities($code) . '</name>';
        $kml[] = '    <description>';
        $kml[] = ' <![CDATA[ ';
        $kml[] = '    <h2> ' . htmlentities($title) . '</h2>';
        $kml[] = '    <h3>Project Manager: <i>' . htmlentities($owner) . '</i></h3><p>US Link: <a href=' . $URLus . ' target="_blank">Project on US</a></p>';
        $kml[] = ' ]]> ';
        $kml[] = ' </description>';
        $kml[] = '    <styleUrl></styleUrl>';
        $kml[] = '    <Point>';
        $kml[] = '        <coordinates>' . $long . ',' . $lat . '</coordinates>';
        $kml[] = '    </Point>';
        $kml[] = ' </Placemark>';
    }

    $kml[] = ' </Document>';
    $kml[] = '</kml>';
    $kmlOutput = join("\n", $kml);

    $myfile = fopen("data/surveyjobs.kml", "w") or die("Unable to open file!");
    fwrite($myfile, $kmlOutput);
    fclose($myfile);
}